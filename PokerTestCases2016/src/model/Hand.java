package model;

import java.util.Vector;

public class Hand
{
	private int myMaxNumberCards;
	private Vector<Card> myCards;

	public Hand(int maxNum)
	{
		myMaxNumberCards = maxNum;
	}

	public boolean add(Card card)
	{
		if (myCards.size() + 1 > myMaxNumberCards)
		{
			return false;
		}
		else
		{
			myCards.add(card);
			return true;
		}
	}

	public Vector<Card> discard(Vector<Integer> indices)
	{
		return null;
	}

	public String toString()
	{
		return myCards.toString();
	}

	public void orderCards()
	{
	}

	public int getNumberCardsInHand()
	{
		return myCards.size();
	}

	public Vector<Card> getCards()
	{
		return myCards;
	}
}
