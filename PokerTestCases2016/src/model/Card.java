package model;

import java.awt.Image;

public class Card 
{
  private CardSuit mySuit;
  private CardType myType;
  private Image myImage;
  private boolean myIsSelected;
  private boolean myIsFaceUp;

  public Card(CardSuit suit, CardType type, Image image) 
  {
	  mySuit = suit;
	  myType = type;
	  myImage = image;
  }

  public void flip() 
  {
	  if (myIsFaceUp == true)
	  {
		  myIsFaceUp = false;
	  }
	  else
	  {
		  myIsFaceUp = true;
	  }
  }

  public boolean isFaceUp() 
  {
	  return myIsFaceUp;
  }

  public boolean isSelected() 
  {
	  return myIsSelected;
  }

  public void toggleSelected() 
  {
	  if (myIsSelected == true)
	  {
		  myIsSelected = false;
	  }
	  else
	  {
		  myIsSelected = true;
	  }
  }

  public int compareTo(Card card) 
  {
	  /*switch (myType.)
	  {
	  		
	  }*/
	  return 0;
  }

  public Object clone() throws CloneNotSupportedException 
  {
	  return super.clone();
  }

  public String toString() 
  {
	  return null;
  }

  public CardSuit getSuit()
  {
	  return mySuit;
  }
  
  public CardType getType()
  {
	  return myType;
  }
  
  public Image getImage()
  {
	  return myImage;
  }
}