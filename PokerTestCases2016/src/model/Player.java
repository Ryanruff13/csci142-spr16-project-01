package model;

public class Player
{
	private static final String DEFAULT_NAME = "JohnCena";
	private String myName;
	private int myNumberWins;
	private boolean myAmAI;
	private PokerHand myHand;

	public Player()
	{
		myName = DEFAULT_NAME;
		myNumberWins = 0;
		myAmAI = false;
		myHand = new PokerHand(0);
	}
	
	public Player(String name)
	{
		if (validateName(name) == true)
		{
			myName = name;
		}
		else
		{
			myName = DEFAULT_NAME;
		}
		myNumberWins = 0;
		myAmAI = false;
		myHand = new PokerHand(0);
	}

	public boolean validateName(String name)
	{
		if (name != "")
		{
			return true;
		}
		return false;
	}

	public int incrementNumberWins()
	{
		return ++myNumberWins;
	}

	public String toString()
	{
		if (myNumberWins == 1)
		{
			return myName + " currently has one win.";
		}
		else
		{
			return myName + " currently has " + myNumberWins + " wins.";
		}
	}

	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	public PokerHand getHand()
	{
		return myHand;
	}

	public String getName()
	{
		return myName;
	}

	public int getNumberWins()
	{
		return myNumberWins;
	}

	public boolean getAmAI()
	{
		return myAmAI;
	}

}
