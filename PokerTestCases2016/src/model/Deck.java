package model;

import java.util.Vector;

public class Deck
{
	private Vector<Card> myCards;
	private int myFullDeckSize;

	public Deck()
	{
		myFullDeckSize = 52;
	}

	public boolean constructDeck()
	{
		CardType cardType = null;
		for (int i = 2; i <= 52; i++)
		{
			Card card = new Card(CardSuit.values()[i], cardType.values()[i % 4], null);
			myCards.add(card);
		}
		return true;
	}

	public Card draw()
	{
		Card cardToReturn = myCards.firstElement();
		myCards.remove(0);
		return cardToReturn;
	}

	public boolean shuffle()
	{
		Card cardToMove;
		for (int i = 0; i < myCards.size(); i+= 2)
		{
			cardToMove = myCards.elementAt(i);
			myCards.removeElementAt(i);
			myCards.insertElementAt(cardToMove, i + 1);
		}
		return true;
	}
	
	public int getFullDeckSize()
	{
		return myFullDeckSize;
	}
	
	public Vector<Card> getCards()
	{
		return myCards;
	}

	public String toString()
	{
		return myCards.toString();
	}

	public Object clone()
	{
		try
		{
			return super.clone();
		}
		catch (CloneNotSupportedException e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
