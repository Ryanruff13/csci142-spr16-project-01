package model;

public class PokerHand extends Hand
{
	private int myNumberCards;
	private int myMaxNumberCards;

	public PokerHand(int maxNum)
	{
		super(maxNum);
	}

	public PokerHandRanking determineRanking()
	{
		return PokerHandRanking.FLUSH;
	}

	public int compareTo(PokerHand pokerHand)
	{
		return 0;
	}

	public String toString()
	{
		return "Number of Cards: " + myNumberCards + ", Maximum Number of Cards: " + myMaxNumberCards;
	}

	public int getRanking()
	{
		return 0;
	}

	public int getNumberCards()
	{
		return myNumberCards;
	}

	public int getMaxNumberCards()
	{
		return myMaxNumberCards;
	}

	public boolean isHighCard()
	{
		return false;
	}

	public boolean isPair()
	{
		return false;
	}

	public boolean isTwoPair()
	{
		return false;
	}

	public boolean isThreeOfKind()
	{
		return false;
	}

	public boolean isStraight()
	{
		return false;
	}

	public boolean isFlush()
	{
		return false;
	}

	public boolean isFullHouse()
	{
		return false;
	}

	public boolean isFourOfKind()
	{
		return false;
	}

	public boolean isStraightFlush()
	{
		return false;
	}

	public boolean isRoyalFlush()
	{
		return false;
	}

}
