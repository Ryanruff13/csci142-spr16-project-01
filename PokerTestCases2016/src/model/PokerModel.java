package model;

public class PokerModel
{
	private Player[] myPlayer;
	private short myIndexPlayerup;
	private int myMaxRounds;
	private int myRound;
	private Deck myDeck;

	public PokerModel(Player player)
	{
		myPlayer = new Player[4];
		myMaxRounds = 10;
		myRound = 1;
		myDeck = new Deck();
	}

	public int switchTurns()
	{
		if (myRound + 1 > myPlayer.length)
		{
			myRound = 1;
		}
		else
		{
			myRound = myRound++ % myPlayer.length;
		}
		return myRound;
	}

	public void dealCards()
	{
		for (short i = 0; i < myPlayer.length; i++)
		{
			
		}
	}

	public Player determineWinner()
	{
		return null;
	}

	public boolean resetGame()
	{
		myPlayer = new Player[4];
		myMaxRounds = 10;
		myRound = 1;
		myDeck = new Deck();
		return true;
	}

	public Player getPlayerUp()
	{
		return null;
	}

	public Player getPlayer(int index)
	{
		return null;
	}

	public int getIndexPlayerUp()
	{
		return myIndexPlayerup;
	}

}
